﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Prova.Models
{
    public class Entidade
    {
        [Key]
        public int idEntidade { get; set; }
        public string fantasia { get; set; }

        public int? idEndereco { get; set; }
        [ForeignKey("idEndereco")]
        public virtual Endereco Endereco { get; set; }
    }
}